'''
DOCSTRING
'''
import os
import glob
import json
ANIME_DB = '../../AnimeDrawingsDataset/data'
'''
dummy = {
    "joints_vis": [
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1
    ],
    "joints": [
        [
            804.0,
            711.0
        ],
        [
            816.0,
            510.0
        ],
        [
            908.0,
            438.0
        ],
        [
            1040.0,
            454.0
        ],
        [
            906.0,
            528.0
        ],
        [
            883.0,
            707.0
        ],
        [
            974.0,
            446.0
        ],
        [
            985.0,
            253.0
        ],
        [
            982.7591,
            235.9694
        ],
        [
            962.2409,
            80.0306
        ],
        [
            869.0,
            214.0
        ],
        [
            798.0,
            340.0
        ],
        [
            902.0,
            253.0
        ],
        [
            1067.0,
            253.0
        ],
        [
            1167.0,
            353.0
        ],
        [
            1142.0,
            478.0
        ]
    ],
    "image": "005808361.jpg",
    "scale": 4.718488,
    "center": [
        966.0,
        340.0
    ]
}
'''
# joint id(0 - r ankle, 1 - r knee, 2 - r hip, 3 - l hip, 4 - l knee, 5 - l ankle, 6 - pelvis, 7 - thorax, 8 - upper neck, 9 - head top, 10 - r wrist, 11 - r elbow, 12 - r shoulder, 13 - l shoulder, 14 - l elbow, 15 - l wrist)
# mapping
mapping = {
    'body_upper': 6,
    'neck': 8,
    'head': 9,
    'nose_top': None,
    'nose_root': None,
    'arm_left': 13,
    'elbow_left':   14,
    'wrist_left': 15,
    'thumb_left': None,
    'arm_right': 12,
    'elbow_right': 11,
    'wrist_right': 10,
    'thumb_right': None,
    'leg_left': 3,
    'knee_left': 4,
    'ankle_left': 5,
    'tiptoe_left': None,
    'leg_right': 2,
    'knee_right': 1,
    'ankle_right': 0,
    'tiptoe_right': None,
    'nose_tip': None,
    'width': None,
    'height': None}


def process_data(dtype="train"):
    """process_data

    :param dtype:
    """
    train_annot = os.path.join(ANIME_DB, '{}.json'.format(dtype))
    with open(train_annot) as anno_file:
        annotations = json.load(anno_file)
    new_annotation = []
    for anno in annotations:
        # check if image is exists
        if not os.path.exists(os.path.join(ANIME_DB, "..", anno["file_name"])):
            print("file {} not found".format(anno["file_name"]))
            continue
        # build joins and joints_vis
        joints = []
        for idx in range(16):
            joints.append([-1, -1])
        joints_vis = [0] * 16
        for joint, value in anno["points"].items():
            if mapping[joint] is not None:
                joints[mapping[joint]] = value
                joints_vis[mapping[joint]] = 1
        obj = {
            "joints": joints,
            "joints_vis": joints_vis,
            # get only image name
            "image": os.path.split(anno["file_name"])[-1],
            "scale": anno['height']/200,
            "center": [anno['width']/2, anno['height']/2]
        }
        new_annotation.append(obj)
        # if len(new_annotation) == 2:
        #     break
    print("Complete {} with {} samples".format(dtype, len(new_annotation)))
    with open('{}.json'.format(dtype), 'w') as outfile:
        json.dump(new_annotation, outfile, indent=4)


def main():
    """main"""
    process_data("train")
    process_data("test")
    process_data("val")


if __name__ == '__main__':
    main()
