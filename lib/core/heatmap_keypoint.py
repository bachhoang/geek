import multiprocessing as mp
import copy
import numpy as np


def extract_local_max(mask_img, num_points, info, heatmap_value_threshold=0.5,
                      close_point_suppression=False, gap=10):
    """extract_local_max

    :param mask_img:
    :param num_points:
    :param info:
    :param heatmap_value_threshold:
    :param close_point_suppression:
    :param gap:
    """
    mask = copy.deepcopy(mask_img)
    height, width = mask.shape
    points = []

    for point_index in range(num_points):
        index = np.argmax(mask)
        y, x = np.unravel_index(index, mask.shape)
        max_value = mask[y, x]
        if max_value <= heatmap_value_threshold:
            return points

        points.append([int(x), int(y)] + info + [max_value, ])

        maximum_suppression(mask, x, y, heatmap_value_threshold)
        if close_point_suppression:
            mask[max(y - gap, 0):min(y + gap, height - 1),
                 max(x - gap, 0):min(x + gap, width - 1)] = 0

    return points


def maximum_suppression(mask, x, y, heatmap_value_threshold):
    """maximum_suppression

    :param mask:
    :param x:
    :param y:
    :param heatmap_value_threshold:
    """
    height, width = mask.shape
    value = mask[y][x]
    mask[y][x] = -1
    deltas = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    for delta in deltas:
        neighbor_x = x + delta[0]
        neighbor_y = y + delta[1]
        if neighbor_x < 0 or neighbor_y < 0 or neighbor_x >= width or neighbor_y >= height:
            continue
        neighbor_value = mask[neighbor_y][neighbor_x]
        if neighbor_value <= value and neighbor_value > heatmap_value_threshold:
            maximum_suppression(mask, neighbor_x, neighbor_y,
                                heatmap_value_threshold)
            pass
        continue


def heatmap_to_keypoints(heatmap_img):
    keypoints = extract_local_max(heatmap_img, 100, [0, 0])
    return keypoints


def f_measure(prediction, target, hm_type='gaussian', thr=0.5, multiprocess=True):
    """f_measure

    :param prediction:
    :param target:
    :param hm_type:
    :param thr:
    :param multiprocess:
    """

    height, width = prediction.shape
    kp_pred = np.asarray(heatmap_to_keypoints(prediction))
    kp_target = heatmap_to_keypoints(target)
    tp = 0.0
    no_target = float(len(kp_target))
    # print("Target", np.sum(target), no_target, np.sum(target) / no_target)
    # if predict something and target is not zero 
    if(kp_pred.shape[0] > 0):
        value = target[(kp_pred[:, 1].astype(int), kp_pred[:, 0].astype(int))]
        tp = np.where(value > 0.5, 1, 0).sum()
        precision = tp / len(value)
    elif(len(kp_target) == 0):
        return 2.0, []  # set a 2.0 for invalid value , to be remove later
    else:
        return 0.0, []

    recall = tp / no_target if no_target > 0.0 else 0.0
    f1 = 2 * float(recall * precision) / float(recall + precision) if (recall + precision != 0) else 0.0
    preds = kp_pred  # store output points
    if np.isnan(f1): # debug Nan
        from fpdb import fpdb; fpdb().set_trace() 

    return f1, preds


def to_tuple(pred, target):
    arr = []
    # iter over batch
    for x, y in zip(pred, target):
        # iter over feature layer
        for xx, yy in zip(x, y):
            arr.append((xx, yy))
    return arr


def chunk(l, c):
    """chunk

    :param l: list
    :param c: chunk size
    """
    for i in range(0, len(l), c):
        yield l[i:i + c]


def cal_f1(pred, target, worker=4, multiprocess=True):
    """cal_f1

    :param pred:
    :param target:
    :param worker:
    """
    batch_size, layer, height, width = pred.shape
    tmp = to_tuple(pred, target)
    results = []
    preds = []
    if multiprocess:
        with mp.Pool(processes=worker) as pool:
            results, preds = zip(*pool.starmap(f_measure, tmp))
    else:
        for arg1, arg2 in tmp:
            re, pe = f_measure(arg1, arg2)
            results.append(re)
            preds.append(pe)

    preds = list(chunk(preds, layer))
    results = np.asarray(results)
    results = np.delete(results, np.where(results == 2.0))
    f1_batch = sum(results) / len(results)
    return f1_batch, preds


def test():
    """test"""
    import matplotlib
    matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt
    import cv2
    # ======================= TEST 1
    height = 256
    width = 256
    data = np.zeros([height, width])
    peak_number = 10
    peak_array = (np.random.rand(peak_number, 2) * height).astype(int)
    for i in range(peak_number):
        data[peak_array[i, 0], peak_array[i, 1]] = 1
    dst = cv2.GaussianBlur(data, (7, 7), 0)
    range_value = dst.max() - dst.min()
    dst = dst * (1 / range_value)
    fig, ax = plt.subplots(dpi=200)
    ax.imshow(dst)
    ax.set_title("input")
    plt.show()
    print("input \n", peak_array)
    kp = heatmap_to_keypoints(dst)
    print("output \n", kp)
    if(len(peak_array) == len(kp)):
        print("PASS")
    else:
        print("FAIL")

    # ======================= TEST 2
    import time
    batch = 2
    layer = 13
    height = 256
    width = 256
    data = np.random.random((batch, layer, height, width)).astype(np.float32)
    start = time.time()
    results, preds = cal_f1(data, data, 1)
    print("time enlapsed with p = 1", time.time() - start)
    start = time.time()
    results, preds = cal_f1(data, data, 8)
    print("time enlapsed with p = 8", time.time() - start)


if __name__ == "__main__":
    test()
